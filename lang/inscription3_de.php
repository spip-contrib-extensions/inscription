<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/inscription3?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_confirmer' => 'Bestätigen',
	'activation_compte' => 'Aktivieren Sie Ihren Zugang',
	'admin' => 'Administrator',
	'afficher_tous' => 'Alle Benutzer anzeigen',
	'ajouter_adherent' => 'Neuen  Benutzer anlegen',
	'aucun' => 'Keiner',
	'aucun_resultat_recherche' => 'Kein Suchergebnis',
	'autre' => 'Andere',

	// B
	'bouton_suppression_compte' => 'Ihren Zugang löschen',

	// C
	'cfg_description' => 'Zusatzfelder und andere Funktionen konfigurieren',
	'cfg_titre' => 'Anmeldung 3',
	'choix_affordance_email' => 'Email',
	'choix_affordance_libre' => 'Feie Wahl (siehe unten)',
	'choix_affordance_login' => 'Login (SPIP-Standard)',
	'choix_affordance_login_email' => 'Login und Email',
	'choix_feminin' => 'Frau',
	'choix_inscription_texte_aucun' => 'n.a.',
	'choix_inscription_texte_libre' => 'Feie Wahl (siehe unten)',
	'choix_inscription_texte_origine' => 'Grundeinstellung (SPIP Standard)',
	'choix_masculin' => 'Herr',
	'compte_active' => 'Ihr Zugang zu @nom_site@',
	'configuration' => 'Konfiguration der Benutzerfelder',
	'contacts_personnels' => 'Persönliche Kontakte',

	// D
	'delete_user_select' => 'Markierte Benutzer löschen',
	'descriptif_page_inscription' => 'Anmeldung bei @site@',
	'descriptif_plugin' => 'Hier sehen Sie alle Benutzer der Website. Ihr Status wird durch ein farbiges Icon angezeigt.<br /><br />Sie können Zusatzfelder aktivieren, die Besuchern bei der Anmeldung zur Verfügung gestellt werden.',
	'divers' => 'Verschiedenes',

	// E
	'email_bonjour' => 'Guten Tag @nom@,',
	'erreur_chaine_valide' => 'Bitte fügen Sie eine Zeichenkette ein',
	'erreur_chainelettre' => '(besteht nur aus Buchstaben)',
	'erreur_chainenombre' => '(besteht aus Buchstaben und/oder Ziffern)',
	'erreur_champ_obligatoire' => 'Pflichtfeld',
	'erreur_compte_attente' => 'Ihr Zugang muss noch freigeschaltet werden.',
	'erreur_compte_attente_mail' => 'Diese Adresse gehört zu einem ungültigen Zugang.',
	'erreur_effacement_auto_impossible' => 'Der Zugang kann nicht automatisch gelöscht werden. Bitte kontaktieren Sie uns.',
	'erreur_info_statut' => 'Der Benutzer @nom@ hat den Status "@statut@".',
	'erreur_inscription_desactivee' => 'Die Anmeldung bei dieses Website ist deaktiviert.',
	'erreur_login_deja_utilise' => 'Dieses Login ist bereits vergeben. Bitte wählen Sie ein anderes.',
	'erreur_naissance_futur' => 'Sind Sie wirklich in der Zukunft geboren ?',
	'erreur_naissance_moins_cinq' => 'Sind Sie wirklich jünger als 5 Jahre ?',
	'erreur_naissance_plus_110' => 'Sind Sie wirklich älter als 110 Jahre ?',
	'erreur_numero_valide' => 'Bitte geben Sie eine gültige Nummer an.',
	'erreur_numero_valide_international' => 'Diese Nummer muß im internationalen Format sein (z.B. : +32 475 123 456)',
	'erreur_reglement_obligatoire' => 'Sie müssen die Regelungen anerkennen.',
	'erreur_signature_deja_utilise' => 'Dieser Wert wird bereits von einem anderen Benutzer verwendet.',
	'erreur_suppression_compte_connecte' => 'Sie müssen bei der Website eingelogt sein, um Ihr Konto zu löschen.',
	'erreur_suppression_compte_non_auteur' => 'Sie sind nicht berechtigt, dieses Konto zu löschen.',
	'erreur_suppression_compte_webmestre' => 'Sie können kein Webmasterkonto löschen.',
	'erreur_suppression_comptes_impossible' => 'Löschen des Kontos fehlgeschlagen',
	'exp_statut_rel' => 'Dieses Feld unterscheidet sich vom SPIP-Status und dient der internen Kontrolle einer Institution.',
	'explication_admin_notifications' => 'Auswahl des/der zu benachrichtigenden Administrator/en',
	'explication_affordance_form' => 'Feld wird auf den Login-Formularen angezeigt (#LOGIN_PUBLIC)',
	'explication_auto_login' => 'Wenn das Passwort im Formular eingegeben wird, erfolgt ein automatisches Login des Nutzer nach dem Anlegen des Kontos.',
	'explication_creation' => 'Speichert das Datum des Anlegens des Kontos.',
	'explication_info_internes' => 'Diese Optionen werden in der Datenbank gespeichert jedoch nicht auf den Formularen für neue Nutzer angezeigt.',
	'explication_inscription_texte' => 'Sichtbarer EInführungstexte am Beginn des Anmeldeformulars',
	'explication_modifier_logo_auteur' => 'Machen Sie einen Doppelklick auf das Logo links, um es zu bearbeiten.',
	'explication_password_complexite' => 'Keine Überprüfung mit Javascript für das Passwort, welches die Nutzer bei der Anmeldung eintragen.',
	'explication_reglement_article' => 'Der Artikel "<a href="@url@" class="spip_in">@titre@</a>" wird für die Beschreibung der Nutzungsbedingungen verwendet.',
	'explication_statut' => 'Wählen Sie den Status, den Sie neuen benutzern zuweisen möchten.',
	'explication_suppression_compte' => 'Bestätigen Sie das Löschen Ihres Kontos (@nom@ - @email@)',
	'explication_valider_compte' => 'Mitgliedschaften müssen von einem Admin bestätigt werden, bevor sie verwendet werden können.',

	// F
	'fiche_adherent' => 'Nutzerblatt',
	'fiche_expl' => 'Das Feld wird auf dem Nutzerblatt angezeigt (öffentlich sichtbare Autorenseite)',
	'fiche_mod_expl' => 'Dieses Feld kann im öffentlichen Bereich der Website vom Mitglied bearbeitet werden, wenn ein Formular zur Bearbeitung des Profils (#FORMULAIRE_EDITER_AUTEUR) oder das Plugin "Der Stift" zum Einsatz kommen.',
	'form_expl' => 'Das Feld wird auf dem Formular #FORMULAIRE_INSCRIPTION angezeigt.',
	'form_oblig_expl' => 'Angabe wird auf den Anmelde- und Änderungsformularen obligatorisch sein.',
	'form_retour_aconfirmer' => 'Ihr Konto wurde angelegt und wartet nun auf dieBestätigung durch einen Administrator.',
	'form_retour_inscription_pass' => 'Ihr Konto wurde eingerichtet. Sie können es sofort verwenden, indem Sie sich mit ihrer Mailadresse anmelden.',
	'form_retour_inscription_pass_logue' => 'Ihr Konto wurde angelegt. Sie sind zur Zeit mit ihm eingelogt.',
	'formulaire_inscription' => 'Anmeldeformular',
	'formulaire_inscription_ok' => 'Ihr Anmeldung wurde gespeichert. Sie erhalten Ihre Zugangsdaten per Email.',
	'formulaire_remplir_obligatoires' => 'Bitte füllen Sie die Pflichtfelder aus.',
	'formulaire_remplir_validation' => 'Bitte überprüfen Sie die ungültigen Felder.',

	// I
	'icone_afficher_utilisateurs' => 'Benutzer anzeigen',
	'icone_configurer_inscription3' => 'Anmeldung 3 konfigurieren',
	'info_aconfirmer' => 'zu bestätigen',
	'infos_personnelles' => 'Persönliche Informationen',

	// L
	'label_login' => 'Nutzername (Login)',
	'label_logo_auteur' => 'Logo',
	'label_mobile' => 'Mobiltelefon:',
	'label_naissance' => 'Geburtsdatum',
	'label_nom' => 'Signatur',
	'label_nom_famille' => 'Familienname',
	'label_nom_site' => 'Name der Website',
	'label_pass' => 'Passwort',
	'label_password_complexite' => 'Komplexität des Passworts prüfen',
	'label_password_retaper' => 'Passwort bestätigen',
	'label_pays' => 'Land',
	'label_pays_defaut' => 'Standard-Land',
	'label_pgp' => 'PGP Schlüssel',
	'label_prenom' => 'Vorname',
	'label_profession' => 'Beruf',
	'label_public_reglement' => 'Ich habe die Regeln gelesen und akzeptiert.',
	'label_reglement' => 'Regeln bestätigen',
	'label_reglement_article' => 'Original-Artikel mit den Website-Regeln',
	'label_reglement_explication' => 'Kasten mit Regeln anzeigen und Bestätigung erzwingen',
	'label_secteur' => 'Bereich',
	'label_website' => 'Website:',
	'legend_oubli_pass' => 'Kein Passwort / Passwort vergessen',
	'legende' => 'Legende',
	'legende_affordance_form' => 'Login-Formular',
	'legende_cextras' => 'Zusatzfelder',
	'legende_formulaire_inscription' => 'Anmeldeformular',
	'legende_info_defaut' => 'Pflichtinformationen',
	'legende_info_internes' => 'Interne Informationen',
	'legende_info_perso' => 'Persönliche Informationen',
	'legende_password' => 'Passwort',
	'legende_reglement' => 'Website-Regeln',
	'legende_validation' => 'Bestätigung',
	'lisez_mail' => 'Ein Email wurde an die angegebene Adresse geschickt. Bitte folgen Sie den Anweisungen, um Ihr Konto zu aktivieren.',
	'liste_adherents' => 'Mitgliederliste anzeigen',
	'liste_comptes_titre' => 'Nutzerliste',

	// M
	'menu_info_inscription3' => 'Link zur Anmeldeseite der Website',
	'menu_nom_inscription3' => 'Link zur Anmeldung',
	'menu_titre_lien_inscription' => 'Anmeldung',
	'message_auteur_inscription_confirmer_contenu_admin' => '@nom@ möchte ein Konto bei dieser Website. Sie können die Anfrage ablehnen oder bestätigen.',
	'message_auteur_inscription_confirmer_contenu_user' => 'Ihr Konto muss noch durch einen Adminsutrator bestätigt werden.',
	'message_auteur_inscription_confirmer_titre_admin' => '[@nom_site_spip@] Bestätigungsanfrage des Kontos @nom@',
	'message_auteur_inscription_confirmer_titre_user' => '[@nom_site_spip@] Ihr Konto wartet auf seine Bestätigung',
	'message_auteur_inscription_pass' => 'Ihr Konto wurde angelegt. Sie haben Ihr Passwort selbst gewählt.',
	'message_auto' => '(dies ist eine automatische Nachricht)',
	'message_compte_efface' => 'Ihr Konto wurde gelöscht.',
	'modif_pass_titre' => 'Passwort ändern',
	'mot_passe_reste_identique' => 'Ihr Passwort wurde geändert.',

	// N
	'nom_explication' => 'ihr Name oder Pseudonym',

	// P
	'probleme_email' => 'Mailproblem: Die Aktivierungsmail kann nicht verschickt werden.',
	'profil_droits_insuffisants' => 'Sie sind nicht berechtigt, diesen Nutzer zu bearbeiten<br/>',
	'profil_modifie_ok' => 'Die Änderungen Ihres Profils wurden gespeichert.',

	// R
	'raccourcis' => 'Abkürzung',
	'recherche_case' => 'Im Feld:',
	'recherche_utilisateurs' => 'Nutzer suchen',

	// S
	'statut_rel' => 'Interner Status',
	'statuts_actifs' => 'Die Farbe der Icons entspricht folgendem Status: ',
	'supprimer_adherent' => 'Nutzer löschen',

	// T
	'table_expl' => 'Dieses Feld wird in der Nutzerliste angezeigt (im Redaktionssystem)',
	'texte_email_confirmation' => 'Ihr Konto wurde aktiviert. Sie können sich nun bei unserer Website mit Ihren Zugangsdaten einloggen.

Ihr Login lautet : @login@
Sie haben Ihr Passwort gerade gewählt.

Danke für Ihr Vertrauen,

Das team von @nom_site@
@url_site@',
	'texte_email_inscription' => 'Sie können die Einschreibung bei der Website @nom_site@ bestätigen.

Klicken Sie auf den Link weiter unten, um Ihr Konto zu aktivieren und Ihr Passwort zu wählen.

@link_activation@


Vieln Dank für Ihr vertrauen.

Das Team von @nom_site@.
@url_site@

Wenn Sie sich nicht eingetragen haben 
oder Ihr Konto bei unserer Website löschen möchten, kliken Sie bitte auf den Link weiter unten. 

@link_suppresion@


',
	'thead_fiche_mod' => 'Änderbar',
	'thead_form' => 'Formular',
	'thead_obligatoire' => 'Obligatorisch',
	'thead_table' => 'Tabelle',
	'titre_modifier_auteur' => 'Profil dieses Nutzers bearbeiten',
	'titre_modifier_auteur_nom' => 'Profil @nom@ bearbeiten',
	'titre_modifier_profil' => 'Ihr Profil bearbeiten',
	'titre_supprimer_compte' => 'Ihr Konto löschen',

	// V
	'vos_articles_auteur' => 'Ihre Artikel',
	'vos_contacts_personnels' => 'Persönliche Kontaktdaten',
	'votre_adresse' => 'Persönliche Adresse',
	'votre_login_mail' => 'Login oder Email :',
	'votre_mail' => 'Email :',
	'votre_nom_complet' => 'Vollständiger Name'
);
